# SheetMusicBot

Search various websites for music scores

## Installation

These are installation instructions for Linux, if you're on a different operating system, you might need to tweak some things. 

You will need `python3` version `3.6` or higher. If you are on a Debian based distribution you also need to `sudo apt install python3-venv`. You will also need a [Google Programmable Search Engine](https://programmablesearchengine.google.com/about/)

#### Setting up the environment

Start by cloning this repository
```
git clone https://gitlab.com/t3Y/sheetmusicbot.git
```

Set up the virtual environment
```
cd sheetmusicbot
python3 -m venv venv
```

Install dependencies in the virtual environment
```
source venv/bin/activate
python3 -m pip install -r requirements.txt
deactivate
```

#### Secrets

Fill in your secrets. Create a file `secrets.txt` with the following content:
```
BOT_TOKEN=
GOOGLE_PSE_KEY=
GOOGLE_PSE_ID=
```
And add each key/token after each respective `=`. The PSE Key and ID can be found in your Google Application and Programmable Search Engine page respectively, the bot token in your Discord Application Bot tab.

## Usage


#### Running the bot

Start the virtual environment with
```
source venv/bin/activate
```
and run the bot with
```
python3 sheetmusicbot.py
```

Options and help can be displayed with `python3 sheetmusicbot.py -h`:
```
usage: sheetmusicbot.py [-h] [-v] [-d] [-l]

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  Set the logging level to INFO
  -d, --debug    Set the logging level to DEBUG
  -l, --log      Save logs
```

The save logs option doesn't do anything yet.

#### Inviting the bot to your server

Your bot's invite link is
```
https://discord.com/api/oauth2/authorize?client_id=<CLIENT_ID>&scope=bot&permissions=116800
```
Where `<CLIENT_ID>` is your Discord application client ID. The bot doesn't need most of those permissions, so feel free to adjust the permission integer.

## Disclaimer

The sheet music files or scans are uploaded by users of [imslp.org](https://imslp.org) and may not necessarily be in the public domain in your jurisdiction. Please acknowledge all Copyright warnings in the bot's command results.

## Acknowledgements

This bot is just the result of cobbling together the following libraries

- [aiohttp](https://github.com/aio-libs/aiohttp/) released under the [Apache 2.0 license](https://github.com/aio-libs/aiohttp/blob/master/LICENSE.txt)
- [discord.py](https://github.com/Rapptz/discord.py/) released under the [MIT license](https://github.com/Rapptz/discord.py/blob/master/LICENSE)
- [beautifulsoup4](https://bazaar.launchpad.net/~leonardr/beautifulsoup/bs4/files) released under the [MIT license](https://bazaar.launchpad.net/~leonardr/beautifulsoup/bs4/view/head:/LICENSE)

and accessing the data hosted by

- [imslp.org](https://imslp.org) (click [here](https://imslp.org/wiki/IMSLP:Site_support) to learn how you can support them)

Many thanks to the contributors of these projects.
