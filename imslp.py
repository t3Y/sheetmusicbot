#!/usr/bin/env python3

#  this file is part of sheetmusicbot
#  Copyright (C) 2020 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import aiohttp
import json
import re
import datetime

from urllib.parse import urljoin
from bs4 import BeautifulSoup


class Imslp:
    def __init__(self, api_key, api_id):
        self._api_key = api_key
        self._api_id = api_id

    async def _get_json(self, terms):
        logging.info("Searching for '{}' on imslp.org".format(terms))
        search_url = ("https://www.googleapis.com/customsearch/v1?key={}&cx={}&num=1&siteSearch=imslp.org&siteSearchFilter=i&q={}"
                      .format(self._api_key, self._api_id, terms))
        async with aiohttp.ClientSession() as session:
            async with session.get(search_url) as r:
                if r.status == 200:
                    return await r.json()
        return None

    async def search(self, terms):
        """
            Returns the link to the first google search for query=terms
            on imslp.org
        """
        raw_data = await self._get_json(terms)
        if raw_data is None:
            return raw_data

        results = raw_data["searchInformation"]["totalResults"]
        if results == "0":
            return "No search results"
        return raw_data["items"][0]["link"] + "#tabScore1"

    async def _rank_by_ratings(self, ratings):
        """
            Determine how good each scan is based on the bayesian avg
            returns a dict containing ID:RANK key:value pairs

            Currently unrated items get the avg rank as a result of this
        """
        DAMPENING = 0.8
        total_rating = 0
        nb_of_ratings = 0
        for key, value in ratings.items():
            total_rating += value[0] * value[1]
            nb_of_ratings += value[1]
        avg_rating = total_rating / max(nb_of_ratings, 1)
        avg_nb_of_ratings = nb_of_ratings / len(ratings)
        fake_ratings = DAMPENING * avg_nb_of_ratings

        bayesian_ratings = []
        for key, value in ratings.items():
            bayesian_rating = (value[0] * value[1] + avg_rating * fake_ratings)
            bayesian_avg = 0
            if bayesian_rating > 0:
                bayesian_avg = bayesian_rating / (value[1] + fake_ratings)
            bayesian_ratings.append({"id": key, "rating": bayesian_avg})

        return await self._rank_by_attribute(lst=bayesian_ratings, attribute="rating", reverse=True)

    async def _rank_by_attribute(self, lst, attribute, reverse):
        lst.sort(key=lambda x: x[attribute], reverse=reverse)
        ranking = {}
        for i in range(len(lst)):
            ranking[lst[i]["id"]] = i+1
        return ranking

    async def _get_all_ratings(self, html):
        """
            Extract the JS object containing ratings from the raw html
        """
        all_ratings = {}
        temp_ratings = None
        try:
            ratings_string = "{" + html.split("IMSLPRatings={")[1].split("}")[0] + "}"
            temp_ratings = json.loads(ratings_string)
        except ValueError:
            logging.warning("Could not find ratings")

        if temp_ratings is not None:
            for key, value in temp_ratings.items():
                all_ratings[key] = [value[0], int(value[1])]
        return all_ratings

    async def pdf(self, terms):
        """
            Tries to extract the best scan from the result of
            self.search(terms), which might not be a page and if it is,
            the page might not necessarily contain scans
        """
        work_link = await self.search(terms)
        if work_link is None or "http" not in work_link:
            return work_link

        async with aiohttp.ClientSession() as session:
            async with session.get(work_link) as r:
                if r.status == 200:
                    raw_html = await r.text()
                    soup = BeautifulSoup(raw_html, "html.parser")
                    score_tab = soup.find(id="tabScore1")
                    if score_tab is None:  # does the page even contain links to scores?
                        return "Could not find any scans, try refining your search"
                    score_divs = score_tab.find_all(class_="we")
                    if score_divs is None:
                        return "Could not find any scans, try refining your search"

                    all_ratings = await self._get_all_ratings(raw_html)  # TODO: Handle the case where parsing ratings failed

                    # Get all IDs and collect downloads, filesize and number of pages
                    downloads = []  # weekly downloads
                    filesizes = []  # filesize in MB
                    ratings = {}    # score ratings
                    copyright = {}
                    nb_of_pages = []
                    score_ids = set()
                    today = datetime.date.today()

                    # A score_div contains one or more scans of an edition
                    # and an edition info table
                    for div in score_divs:
                        scans_info = div.find_all(class_="we_file_info2")
                        for scan in scans_info:
                            # only get complete scores
                            if "Complete Score" not in scan.parent.text:
                                matching_term = False
                                for term in terms.split(" "):
                                    if term.lower() in scan.parent.text.lower():  # does not give the most precise results...
                                        matching_term = True
                                        break
                                if not matching_term:
                                    continue

                            anchors = scan.find_all("a")
                            score_id = anchors[1].string[1:]  # rather than getting anchors by index, checking content might be more robust
                            score_ids.add(score_id)

                            # save the rating corresponding to this ID:
                            ratings[score_id] = all_ratings[score_id.lstrip("0")]

                            # search copyright notice
                            copy = ", ".join(div.find_all(string=re.compile("Non-PD [A-Z]+")))
                            if len(copy) > 0:
                                copyright[score_id] = copy

                            # get the download count if available
                            download = None
                            for anchor in anchors:
                                title = anchor.get("title")
                                if title is not None and "Special:GetFCtrStats" in title:
                                    download = int(anchor.string)

                            if download is not None:
                                upload_date_arr = re.search(r"\([0-9]+/[0-9]+/[0-9]+\)", scan.parent.parent.parent.text).group(0).split("/")
                                year = int(upload_date_arr[0].lstrip("("))
                                month = int(upload_date_arr[1])
                                day = int(upload_date_arr[2].rstrip(")"))
                                upload_date = datetime.date(year, month, day)

                                weeks_since_upload = (today - upload_date).days / 7
                                download = download / weeks_since_upload

                            downloads.append({"id": score_id, "downloads": download})

                            # TODO: Catch and handle exceptions in the following
                            text = scan.text
                            filesize = float(re.search(r"[0-9]+\.?[0-9]*MB", text).group(0).rstrip("MB"))
                            pages = int(re.search(r"[0-9]+ pp", text).group(0).rstrip(" pp"))
                            filesizes.append({"id": score_id, "filesize": filesize})
                            nb_of_pages.append({"id": score_id, "pages": pages})

                    if len(score_ids) == 0:
                        return "Found some scans, but no complete score or parts matching your search terms. Try refining your search"

                    # gives files without download info the avg weekly download count
                    total_downloads = 0
                    entries = 0
                    for entry in downloads:
                        if entry["downloads"] is not None:
                            total_downloads += entry["downloads"]
                            entries += 1

                    avg_downloads = total_downloads / max(1, entries)

                    for entry in downloads:
                        if entry["downloads"] is None:
                            entry["downloads"] = avg_downloads

                    # rank scores by criteria
                    rating_ranking = await self._rank_by_ratings(ratings)
                    downloads_ranking = await self._rank_by_attribute(lst=downloads, attribute="downloads", reverse=True)
                    filesizes_ranking = await self._rank_by_attribute(lst=filesizes, attribute="filesize", reverse=False)

                    ranked_ids = await self._final_ranking(rating_ranking, downloads_ranking, filesizes_ranking, score_ids)

                    available_links = await self._get_direct_links(ranked_ids, 3)

                    formatted_output = []
                    for entry in available_links:
                        link = entry["link"]
                        disclaimer = ""
                        if entry["id"] in copyright.keys():
                            disclaimer = " **{}**".format(copyright[entry["id"]])  # ideally the text formatting should be done in the main cmd
                        formatted_output.append((link, disclaimer))
                    return formatted_output

        return None

    async def _get_direct_links(self, score_ids, cutoff):
        """
            Get a list of id and link pairs for the links that are actually up
            and can get handled by get_direct_link
        """
        output = []
        while len(score_ids) > 0 and len(output) < cutoff:
            score_id = score_ids.pop(0)
            link = await self._get_direct_link(score_id)
            if link is not None:
                output.append({"id": score_id, "link": link})
        return output

    async def _get_direct_link(self, score_id):
        # initial_link will result in a http 200 and a page with the 15
        # second timer, or a 301 redirecting to a disclaimer
        # seems like aiohttp redirects automagically though
        initial_link = "https://imslp.org/wiki/Special:ImagefromIndex/{}/sevqs".format(score_id)
        async with aiohttp.ClientSession() as session:
            cookies = {"imslpdisclaimeraccepted": "yes"}
            async with session.get(initial_link, cookies=cookies) as r:
                if r.status == 200:
                    # check the content type
                    if "application/pdf" in r.headers["content-type"]:
                        return str(r.url)

                    if "text/html" in r.headers["content-type"]:
                        raw_html = await r.text()
                        soup = BeautifulSoup(raw_html, "html.parser")
                        link = soup.find(id="sm_dl_wait")
                        if link is not None:
                            return link["data-id"]
                        link = soup.find("a", string="I agree with the disclaimer above, continue my download")
                        if link is not None:
                            href = link.get("href")
                            if "http" not in href:
                                href = urljoin(str(r.url), href)
                            return href

                logging.warning("Handling of {} not yet implemented (HTTP STATUS{})".format(initial_link, r.status))
                return None

    async def _final_ranking(self, rating, downloads, filesizes, score_ids):
        final_ranking = []
        for score_id in score_ids:
            score = rating[score_id] * 0.2 + downloads[score_id] * 0.3 + filesizes[score_id] * 0.5
            final_ranking.append({"id": score_id, "score": score})
        final_ranking.sort(key=lambda x: x["score"], reverse=False)
        output = []
        for ranking in final_ranking:
            output.append(ranking["id"])

        return output
