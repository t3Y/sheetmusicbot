FROM python:3.8

COPY . .

RUN python3 -m pip install -r ./requirements.txt

CMD [ "python3", "./sheetmusicbot.py", "--secrets", "/config/secrets.txt" ]
