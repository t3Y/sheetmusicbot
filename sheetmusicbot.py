#!/usr/bin/env python3

#  sheetmusicbot
#  Copyright (C) 2020 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging
import argparse
import traceback

import discord
from discord.ext import commands
from auth import load_token, load_google_pse_id, load_google_pse_key
from imslp import Imslp


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="Set the logging level to INFO", action="store_true")
    parser.add_argument("-d", "--debug", help="Set the logging level to DEBUG", action="store_true")
    parser.add_argument("-s", "--secrets", help="Path to the secrets file")
    parser.add_argument("-l", "--log", help="Save logs", action="store_true")  # TODO

    args = parser.parse_args()
    return args


def set_log_level(args):
    log_level = logging.WARNING

    if args.verbose:
        log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S")


def main():

    args = parse_args()
    set_log_level(args)

    TOKEN = load_token(args.secrets)
    GOOGLE_PSE_KEY = load_google_pse_key(args.secrets)
    GOOGLE_PSE_ID = load_google_pse_id(args.secrets)

    if TOKEN is None or GOOGLE_PSE_ID is None or GOOGLE_PSE_KEY is None:
        sys.exit(1)

    imslp = Imslp(GOOGLE_PSE_KEY, GOOGLE_PSE_ID)
    PREFIX = "s!"
    bot = commands.Bot(command_prefix=PREFIX)

    async def signal_error(ctx, message):
        embed = discord.Embed(title="Error", type="rich", colour=discord.Colour.red(), description=message)
        await ctx.send(content=None, embed=embed)

    @bot.event
    async def on_ready():
        logging.info("Logged in as {0.user}".format(bot))
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="{}help".format(PREFIX)))

    @bot.event
    async def on_command_error(ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await signal_error(ctx, "Missing one or more argument(s)")
        else:
            logging.warning(error)
            logging.debug("".join(traceback.format_exception(type(error), error, error.__traceback__)))
            await signal_error(ctx, "Unkown error")

    @bot.command(
            brief="Search for composers or score pages",
            help="Search for the provided string on all source websites, the command will return a link to the first result on each site."
    )
    async def search(ctx, *, arg):
        imslp_res = await imslp.search(arg)
        if imslp_res is not None:

            title = "Search Results for '{}'".format(arg)
            res = discord.Embed(title=title, type="rich", colour=discord.Colour.blue())
            res.add_field(name="imslp.org", value=imslp_res)
            await ctx.send(content=None, embed=res)
        else:
            await signal_error(ctx, "Something went wrong")

    @bot.command(
            brief="Get direct links to pdfs matching the search terms",
            help="""This command takes the result of the search command and searches it for links to pdfs. If there are none, it tries to explain what went wrong.

            By default only Complete Scores are considered on imslp.org, unless the title matches at least one of the search terms.
            The imslp.org results are ranked by a combination of weekly downloads, filesize and rating and only the best scoring scans are returned."""
    )
    async def pdf(ctx, *, arg):
        imslp_res = await imslp.pdf(arg)
        if imslp_res is not None:

            imslp_field = ""
            lines = 1
            if isinstance(imslp_res, str):
                imslp_field = imslp_res
            else:
                for link_tuple in imslp_res:
                    line = "**{}.** [{}]({}) {}\n".format(lines, link_tuple[0].split("/")[-1], link_tuple[0], link_tuple[1])
                    if len(imslp_field) + len(line) > 1024:
                        break
                    imslp_field += line
                    lines += 1

            title = "PDFs for '{}'".format(arg)
            res = discord.Embed(title=title, type="rich", colour=discord.Colour.blue())
            res.add_field(name="imslp.org", value=imslp_field)
            await ctx.send(content=None, embed=res)
        else:
            await ctx.send("whoops something went wrong")

    try:
        bot.run(TOKEN)
    except discord.errors.LoginFailure:
        logging.critical("Invalid bot token")
        sys.exit(1)


main()
