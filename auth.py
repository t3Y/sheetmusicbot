#!/usr/bin/env python3

#  this file is part of sheetmusicbot
#  Copyright (C) 2020 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging


def load_token(secrets_file):
    try:
        if secrets_file is None:
            secrets_file = "./secrets.txt"
        with open(secrets_file) as secrets:
            for line in secrets:
                if "BOT_TOKEN" in line:
                    token = line.split("BOT_TOKEN=")[1]
                    if len(token) > 5:
                        return token.strip()
    except FileNotFoundError:
        logging.critical(
            "Could not find file secrets.txt, creating it. Fill in your secrets before running the bot"
        )
        with open("./secrets.txt", "w") as secrets:
            secrets.write("BOT_TOKEN=\nGOOGLE_PSE_KEY=\nGOOGLE_PSE_ID=")
        return None

    logging.critical(
        "Could not find a bot token in secrets.txt, make sure to add it before running the bot"
    )
    return None


def load_google_pse_key(secrets_file):
    try:
        if secrets_file is None:
            secrets_file = "./secrets.txt"
        with open(secrets_file) as secrets:
            for line in secrets:
                if "GOOGLE_PSE_KEY" in line:
                    token = line.split("GOOGLE_PSE_KEY=")[1]
                    if len(token) > 5:
                        return token.rstrip()
    except FileNotFoundError:
        logging.critical(
            "Could not find file secrets.txt, creating it. Fill in your secrets before running the bot"
        )
        with open("./secrets.txt", "w") as secrets:
            secrets.write("BOT_TOKEN=\nGOOGLE_PSE_KEY=\nGOOGLE_PSE_ID=")
        return None

    logging.critical(
        "Could not find a Google PSE api key in secrets.txt, make sure to add it before running the bot"
    )
    return None


def load_google_pse_id(secrets_file):
    try:
        if secrets_file is None:
            secrets_file = "./secrets.txt"
        with open(secrets_file) as secrets:
            for line in secrets:
                if "GOOGLE_PSE_ID" in line:
                    token = line.split("GOOGLE_PSE_ID=")[1]
                    if len(token) > 5:
                        return token.rstrip()
    except FileNotFoundError:
        logging.critical(
            "Could not find file secrets.txt, creating it. Fill in your secrets before running the bot"
        )
        with open("./secrets.txt", "w") as secrets:
            secrets.write("BOT_TOKEN=\nGOOGLE_PSE_KEY=\nGOOGLE_PSE_ID=")
        return None

    logging.critical(
        "Could not find a Google PSE id in secrets.txt, make sure to add it before running the bot"
    )
    return None
